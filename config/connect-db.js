var mysql = require('mysql');
const ip_address = require("ip").address();

if(ip_address != '103.27.60.44') {
	// Localhost
	var con = mysql.createConnection({
		host: "localhost",
		user: "root",
		password: "",
		database: "web_dev"
	});
}else {
	var con = mysql.createConnection({
		host: "localhost",
		user: "root",
		password: "melody123@AA",
		database: "web_live"
	});
}

con.connect(function(err) {
	if (err) console.log('Cant connect to db, Check ur db connection');
});

module.exports = con;