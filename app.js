var express = require("express"),
    http = require("http"),
    group = require('express-group-routes'),
    path = require('path'),
    session = require('express-session'),
    con = require('./config/connect-db');

var app = express();
app.set('view engine', 'ejs');
app.use('/public', express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: 'secret-session',
    cookie: { maxAge: 60000 }
}));


var pageFrontRouter = require('./routes/pageFrontRouter');
pageFrontRouter(app, __dirname, con);
var pageAdminRouter = require('./routes/pageAdminRouter');
pageAdminRouter(app, __dirname, con);
pageAdminRouter.blogroute(app, __dirname, con);
pageAdminRouter.user(app, __dirname, con);
app.get('*', function(req, res){
    res.redirect('/error-404');
});

var server = http.createServer(app);
server.listen(3000, () => console.log('Server started'));