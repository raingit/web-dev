const path = require('path');

module.exports = function(app, root, con) {
    app.group("/admin", (route) => {

        // Root
        route.get('/', function(req, res) {
            if(!req.session.logged) {
                res.redirect('/admin/login');
                return;
            }
            res.render(root + '/admin/layout.ejs', {
                slug: 'index'
            });
        });

        // Project
        route.get('/project/add', function(req, res) {
            if(!req.session.logged) {
                res.redirect('/admin/login');
                return;
            }
            con.query("SELECT * FROM project_categories", function (err, result, fields) {
                res.render(root + '/admin/layout.ejs', {
                    slug: 'project-add',
                    data_cat: err ? '[]' : JSON.stringify(result)
                });
            });
        }).post('/project/add', function(req, res) {
            var formidable = require('formidable'),
                form = new formidable.IncomingForm(),
                rand_text = Math.random().toString(36).substr(2, 5);

            const render = (error, success) => {
                con.query("SELECT * FROM project_categories", function (dbError, result, fields) {
                    //console.log('Select Error', dbError )
                    res.render(root + '/admin/layout.ejs', {
                        slug: 'project-add',
                        data_cat: dbError ? '[]' : JSON.stringify(result),
                        success,
                        error
                    });
                });
            }
            const result = {}
            form.parse(req, (error, fields, files) => {
                result.fields = fields
            });
            form.on('fileBegin', function (name, file) {
                const filename = rand_text+ file.name
                file.path = path.join(root, 'uploads/project', filename);
                result.image = filename
            });
            form.on('end', () => {
                const { fields, image } = result // giong nhu const fields = result.fields
                var sql = `INSERT INTO project (name, price, vi_description, en_description, ft_img, id_cat) VALUES ('${fields.name}', 'Liên hệ', '${fields.vi_desc}', '${fields.en_desc}', '${image}', '${fields.cat}')`;
                con.query(sql, (error) => {
                    //console.log('Insert error', error)
                    render(error, !error)
                });
            })
            form.on('error', (error) => {
                //console.log('Upload error', error)
                render(error, !error)
            })
        });

        // Categories project
        route.get('/project-category/add', function(req, res) {
            if(!req.session.logged) {
                res.redirect('/admin/login');
                return;
            }
            res.render(root + '/admin/layout.ejs', {
                slug: 'project-cat-add'
            });
        }).post('/project-category/add', function(req, res) {
            // Insert
            var sql = "INSERT INTO project_categories (vi_name, en_name) VALUES ('"+req.body.vi_name+"', '"+req.body.en_name+"')";
            var error = 'no';
            con.query(sql, function (err, result) {
                if (err) { error = 'yes'; }
                // View
                res.render(root + '/admin/layout.ejs', {
                    slug: 'project-cat-add',
                    error: error
                });
            });
        });



    });



}

module.exports.blogroute = function(app, root, con) {
    app.group("/admin", (route) => {
        route.get('/blog/add', function(req, res) {
            if(!req.session.logged) {
                res.redirect('/admin/login');
                return;
            }
            res.render(root + '/admin/layout.ejs', {
                slug: 'blog-add'
            });
        }).post('/blog/add', function(req, res) {
            var formidable = require('formidable'),
                form = new formidable.IncomingForm(),
                rand_text = Math.random().toString(36).substr(2, 5),
                convert_slug = require('../modules/convert-slug.js'),
                datetime = require('node-datetime'),
                datetime = datetime.create(),
                datetime = datetime.format('Y-m-d');

            const render = (error, success) => {
                res.render(root + '/admin/layout.ejs', {
                    slug: 'blog-add',
                    success,
                    error
                });
            }
            const result = {}
            form.parse(req, (error, fields, files) => {
                result.fields = fields
            });
            form.on('fileBegin', function (name, file) {
                const filename = rand_text+ file.name
                file.path = path.join(root, 'uploads/blog', filename);
                result.image = filename
            });
            form.on('end', () => {
                const { fields, image } = result;
                const data = {
                    vi_name: fields.vi_name,
                    en_name: fields.en_name,
                    ft_img: image,
                    vi_content: fields.vi_content,
                    en_content: fields.en_content,
                    created_date: datetime,
                    vi_slug: convert_slug(fields.vi_name),
                    en_slug: convert_slug(fields.en_name)
                }
                var sql = 'INSERT INTO blogs SET ? ';
                con.query(sql, data, (error) => {
                    render(error, !error)
                });
            })
            form.on('error', (error) => {
                render(error, !error)
            })
        });


    });
}

module.exports.user = function(app, root, con) {
    app.group("/admin", (route) => {
        // Login
        route.get('/login', function(req, res) {
            if(req.session.logged) {
                res.redirect('/admin/');
                return;
            }
            res.render(root + '/admin/pages/login.ejs');
        }).post('/login', function(req, res) {
            var sql = `SELECT * FROM users WHERE username = '${req.body.username}'`;
            con.query(sql, function (err, data) {
                // Not in user system
                if(!data.length) {
                    res.render(root + '/admin/pages/login.ejs', {
                        message: 'error'
                    });
                }else {
                    // Check password
                    const bcrypt = require('bcrypt');
                    bcrypt.compare(req.body.password, data[0].password, function(err_check, data_check) {
                        if(data_check) {
                            req.session.logged = true;
                            req.session.username = data[0].username;
                            //res.render(root + '/admin/pages/login.ejs', {
                            //    message: 'success'
                            //});
                            res.redirect('/admin/');
                        } else {
                            res.render(root + '/admin/pages/login.ejs', {
                                message: 'error'
                            });
                        }
                    });
                }
            });
        });




    });
}