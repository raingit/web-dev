module.exports = function(app, root, con) {
    // Pass variable to all ejs view
    app.use(function(req, res, next) {
        var result, lang;
        const ip = require("ip");
        if (req.url.indexOf("/en") != -1){
            lang = 'en';
        }else {
            lang = 'vi';
        }
        // Pass lang variable to locals to check in view
        res.locals.lang = lang;
        res.locals.username = req.session.username;
        res.locals.ip_address = ip.address();
        res.locals.url_home = 'https://vntopweb.com';
        res.locals.trans_text = (slug) => {
            var result;
            var data_trans = require(root +"/data_trans.json");
            data_trans.forEach(function(data) {
                if(data.slug == slug) {
                    if(res.locals.lang == 'vi') {
                        result = data.vn_text;
                    }else {
                        result = data.en_text;
                    }
                }
            });
            return result;
        }
        next();
    });

    app.get('/', function(req, res) {
        res.render(root + '/front/pages/index.ejs', {
            title: res.locals.trans_text('home'),
            page_name : 'home'
        });
    });

    app.get('/en', function(req, res) {
        res.render(root + '/front/pages/index.ejs', {
            title: res.locals.trans_text('home'),
            page_name : 'home'
        });
    });

    app.get('/:language(en)?/blog', function(req, res) {
        var sql = "SELECT * FROM blogs";
        con.query(sql, function (err, data) {
            res.render(root + '/front/pages/blog.ejs', {
                title: res.locals.trans_text('blog'),
                page_name : 'blog',
                data
            });
        });
    });

    app.get('/:language(en)?/blog/:id', function(req, res) {
        var id = req.params.id,
            id = id.split('.')[0];
        var sql = "SELECT * FROM blogs WHERE id = '"+id+"'";
        con.query(sql, function (err, data) {
            if(!data.length) {
                res.locals.lang == 'vi' ? res.redirect('/error-404') : res.redirect('/en/error-404');
            }else {
                res.render(root + '/front/pages/blog-single.ejs', {
                    title: res.locals.lang == 'vi' ? data[0].vi_name : data[0].en_name,
                    page_name :  'blog/'+data[0].id+'.html',
                    data: data || []
                });
            }
        });
    });

    app.get('/:language(en)?/contact', function(req, res) {
        res.render(root + '/front/pages/contact.ejs', {
            title: res.locals.trans_text('contact'),
            page_name : 'contact'
        });
    }).post('/:language(en)?/contact', function(req, res) {
        const nodeMailer = require('nodemailer');
        var content = 'Name: '+ req.body.name + '<br />';
            content += 'Email: '+ req.body.email + '<br />';
            content += 'Subject: '+ req.body.subject + '<br />';
            content += 'Message: '+ req.body.message + '<br />';
        const transporter = nodeMailer.createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
                user: 'hostingserver39@gmail.com',
                pass: 'gypsdeelknwhancs'
            }
        });
        const mailOptions = {
            from: '"Vietnam Top Web" <hostingserver39@gmail.com>',
            to: 'vantan939@gmail.com',
            subject: 'Liên hệ mới từ Contact Form ' + req.body.email,
            text: '',
            html: content,
            replyTo: req.body.email
        };
        transporter.sendMail(mailOptions, (err, info) => {
            var message;
            if (err) {
                message = 'error';
            }else {
                message = 'success';
            }
            res.render(root + '/front/pages/contact.ejs', {
                title: 'Liên hệ',
                page_name : 'contact',
                message
            });
        });
    });

    app.get('/:language(en)?/about', function(req, res) {
        res.render(root + '/front/pages/about.ejs', {
            title: res.locals.trans_text('about_team'),
            page_name : 'about'
        });
    });

    app.get('/:language(en)?/error-404', function(req, res) {
        res.render(root + '/front/pages/error-404.ejs', {
            title: res.locals.trans_text('error_404'),
            page_name : 'error-404'
        });
    });

    app.get('/:language(en)?/project', function(req, res) {
        // List category
        var sql = "SELECT * FROM project_categories";
        con.query(sql, function (errCategories, data_cat) {
            // List project
            var sql = "SELECT * FROM project ORDER BY id DESC";
            con.query(sql, function (errProjects, data_project) {
                res.render(root + '/front/pages/project.ejs', {
                    title: res.locals.trans_text('list_project'),
                    page_name : 'project',
                    data_cat: data_cat || [], // === data_cat: data_cat
                    data_project: data_project || [] // neu errProjects != null tuc la cau select list project loi thi cay ni = null, tuong tu cay data_cat
                });
            });
        });
    });

    app.get('/:language(en)?/project/category/:id', function(req, res) {
        // List category
        var sql = `SELECT * FROM project_categories`;
        con.query(sql, function (errCategories, data_cat) {
            // List project
            var sql = `SELECT * FROM project WHERE id_cat='${req.params.id}'`;
            con.query(sql, function (errProjects, data_project) {
                res.render(root + '/front/pages/project.ejs', {
                    title: res.locals.trans_text('list_project'),
                    page_name : 'project/category/'+req.params.id,
                    data_cat: data_cat || [], // === data_cat: data_cat
                    data_project: data_project || [], // neu errProjects != null tuc la cau select list project loi thi cay ni = null, tuong tu cay data_cat
                    id_project: req.params.id
                });
            });
        });
    });

    app.get('/:language(en)?/project/:id', function(req, res) {
        // Check slug
        var id = req.params.id,
        id = id.split('.')[0];
        var sql = `SELECT *, project.id as p_id FROM project INNER JOIN project_categories ON project.id_cat = project_categories.id WHERE project.id='${id}' `;
        con.query(sql, function (err, data) {
            if(!data.length) {
                res.locals.lang == 'vi' ? res.redirect('/error-404') : res.redirect('/en/error-404');
            }
            res.render(root + '/front/pages/project-detail.ejs', {
                title: res.locals.trans_text('project_detail'),
                page_name : 'project/'+data[0].p_id+'.html',
                data,
                kind_page: 'project-single'
            });
        });
    });

}